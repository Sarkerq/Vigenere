#include<iostream>
using namespace std;
int main()
{
    string klucz;
    string tekst;
    cout<<"Podaj klucz: ";
    getline(cin, klucz);
    cout<<"Podaj tekst: ";
    getline(cin, tekst);
    char antyklucz[10000];
    int antysize=klucz.size();
    int k=0;
    for(int i=0;i<klucz.size();i++)
    {
        if(toupper(klucz[i])<65||toupper(klucz[i])>90)
        {
            antysize--;
            k--;
        }
        else antyklucz[k]=(91-toupper(klucz[i]))%26 +65;
        k++;
    }
    char wynik[10000];
    int j=0;
    for(int i=0; i<tekst.size();i++)
    {
        char temptekst = toupper(tekst[i]);
        char tempklucz = toupper(klucz[j%klucz.size()]);
        while(tempklucz<65||tempklucz>90 )
        {
            j++;
            tempklucz = toupper(klucz[j%klucz.size()]);
        }
        if(temptekst<65||temptekst>90 )
        {
            wynik[i]=temptekst;
            j--;
        }
        else wynik[i]=tolower((temptekst-65 + tempklucz-65)%26 +65);
        j++;
    }
    cout<<wynik<<endl;
    cout<<"Klucz deszyfrujacy: ";
    cout<<antyklucz<<endl;
    cout<<"Odszyforwywanie: ";
    char wynik2[10000];
    j=0;
    for(int i=0; i<tekst.size();i++)
    {
        char temptekst = toupper(wynik[i]);
        char tempklucz = toupper(antyklucz[j%antysize]);
        while(tempklucz<65||tempklucz>90 )
        {
            j++;
            tempklucz = toupper(antyklucz[j%antysize]);
        }
        if(temptekst<65||temptekst>90 )
        {
            wynik2[i]=temptekst;
            j--;
        }
        else wynik2[i]=tolower((temptekst-65 + tempklucz-65)%26 +65);
        j++;
    }
    cout<<wynik2<<endl;
    return 0;
}
